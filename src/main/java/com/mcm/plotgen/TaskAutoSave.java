package com.mcm.plotgen;

import com.mcm.core.api.ThreadAPI;
import com.mcm.plotgen.cache.PlotInUse;
import com.mcm.plotgen.cache.PlotsLocation;
import com.mcm.plotgen.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TaskAutoSave {

    /**
     * Saving all plots
     */
    public static void run() {
        ThreadAPI.createThread(() -> {
            if (PlotInUse.locals != null) {
                for (String local : PlotInUse.locals) {
                    Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, () -> {
                       try {
                           Location pos1 = new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 41, 1, PlotsLocation.get(local).getLocation().clone().getBlockZ() - 75);
                           Location pos2 = new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 191, 256, PlotsLocation.get(local).getLocation().clone().getBlockZ() + 75);
                           WorldEditUtil.save("plugins/mcm-plotsgen/plots/" + PlotInUse.get(local).getUuid(), WorldEditUtil.copy(WorldEditUtil.getRegion(pos1, pos2)));
                           //ADD HERE RABBITMQ/DB SAVE
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (WorldEditException e) {
                            e.printStackTrace();
                        }
                    });
                }
            }
        }, TimeUnit.MINUTES.toMillis(5));
    }
}
