package com.mcm.plotgen.inventoryListeners;

import com.mcm.core.Main;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.HomeDb;
import com.mcm.core.database.TagDb;
import com.mcm.plotgen.cache.PlotInUse;
import com.mcm.plotgen.cache.PlotsLocation;
import com.mcm.plotgen.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import java.io.IOException;

public class InventorySyncPlot implements Listener {

    @EventHandler
    public void onInteractInv(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(com.mcm.core.Main.getTradution("vG!H#f6$&kHb3?U", uuid))) {
            event.setCancelled(true);

            if (event.getSlot() == 12 && event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                String local = String.valueOf(player.getLocation().getBlockX()).substring(0, String.valueOf(player.getLocation().getBlockX()).length() - 3) + ":"
                        + String.valueOf(player.getLocation().getBlockZ()).substring(0, String.valueOf(player.getLocation().getBlockZ()).length() - 3);

                if (PlotInUse.get(local) == null) {
                    if (HomeDb.haveHome(uuid)) {
                        player.getOpenInventory().close();
                        new PlotInUse(local, uuid, HomeDb.getSize(uuid)).insert();

                        try {
                            WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/plots/" + uuid), true, new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 41, 1, PlotsLocation.get(local).getLocation().clone().getBlockZ() - 75));
                        } catch (WorldEditException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        player.sendMessage(Main.getTradution("KbpSwEZYnM%#A58", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("#w9fa$JBfUc9P@U", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }

            if (event.getSlot() == 13 && event.getClickedInventory().getItem(event.getSlot()) != null && !event.getClickedInventory().getItem(event.getSlot()).getType().equals(Material.AIR)) {
                String local = String.valueOf(player.getLocation().getBlockX()).substring(0, String.valueOf(player.getLocation().getBlockX()).length() - 3) + ":"
                        + String.valueOf(player.getLocation().getBlockZ()).substring(0, String.valueOf(player.getLocation().getBlockZ()).length() - 3);

                if (PlotInUse.get(local) == null) {
                    if (CoinsDb.getCoins(uuid) >= com.mcm.plotgen.managers.InventorySyncPlot.plotPrice) {
                        player.getOpenInventory().close();
                        CoinsDb.updateCoins(uuid, CoinsDb.getCoins(uuid) - com.mcm.plotgen.managers.InventorySyncPlot.plotPrice);

                        int size = 100;
                        if (!TagDb.getTag(uuid).equals("membro")) size = 150;
                        HomeDb.createHome(uuid, size);

                        if (!TagDb.getTag(uuid).equals("membro")) {
                            new PlotInUse(local, uuid, 150).insert();
                        } else new PlotInUse(local, uuid, 100).insert();

                        player.sendMessage(Main.getTradution("DuhHw*NgDKtfN6j", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                        try {
                            if (WorldEditUtil.load("plugins/mcm-plotsgen/old-locals/" + local) == null) {
                                Location pos1 = new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 41, 1, PlotsLocation.get(local).getLocation().clone().getBlockZ() - 75);
                                Location pos2 = new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 191, 256, PlotsLocation.get(local).getLocation().clone().getBlockZ() + 75);
                                WorldEditUtil.save("plugins/mcm-plotsgen/old-locals/" + local, WorldEditUtil.copy(WorldEditUtil.getRegion(pos1, pos2)));
                            } else WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/old-locals/" + local), true, new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 41, 1, PlotsLocation.get(local).getLocation().clone().getBlockZ() - 75));
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (WorldEditException e) {
                            e.printStackTrace();
                        }
                    } else {
                        player.sendMessage(Main.getTradution("b3qUyt2@%F4N!hq", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("#w9fa$JBfUc9P@U", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }
}
