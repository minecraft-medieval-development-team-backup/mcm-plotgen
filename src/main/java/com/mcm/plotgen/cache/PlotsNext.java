package com.mcm.plotgen.cache;

import org.bukkit.Location;

import java.util.HashMap;

public class PlotsNext {

    private String distance;
    private Location location;
    private static HashMap<String, PlotsNext> cache = new HashMap<>();

    public PlotsNext(String distance, Location location) {
        this.distance = distance;
        this.location = location;
    }

    public static void delete(String distance) {
        cache.remove(distance);
    }

    public PlotsNext insert() {
        this.cache.put(this.distance, this);
        return this;
    }

    public static PlotsNext get(String distance) {
        return cache.get(distance);
    }

    public Location getLocation() {
        return this.location;
    }
}
