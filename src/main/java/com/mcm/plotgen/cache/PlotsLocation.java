package com.mcm.plotgen.cache;

import com.mcm.core.database.PlotsDb;
import com.mcm.plotgen.utils.WorldEditUtil;
import com.mcm.plotgen.ChunkGenerator;
import com.mcm.plotgen.Main;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

public class PlotsLocation {

    private static String local;
    private static Location location;
    private static HashMap<String, PlotsLocation> cache = new HashMap<>();

    public PlotsLocation(String local, Location location) {
        this.local = local;
        this.location = location;
    }

    public PlotsLocation insert() {
        this.cache.put(this.local, this);
        return this;
    }

    public static PlotsLocation get(String pos) {
        if (cache.get(pos) == null) new PlotsLocation(pos, null).insert();
        return cache.get(pos);
    }

    public Location getLocation() {
        return this.location;
    }

    public void place(Location location) {
        if (this.location != null) return;
        int min = ChunkGenerator.getMin(location, 20);
        this.location = new Location(location.getWorld(), location.getBlockX() + 4, min + 4, location.getZ());
        PlotsDb.addPlotLocation(this.location.getWorld().getName() + ":" + this.location.getBlockX() + ":" + this.location.getBlockY() + ":" + this.location.getBlockZ());
        Bukkit.broadcastMessage(ChatColor.GREEN + "Placed: " + location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ());

        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
            try {
                Location loc = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), location.getYaw(), location.getPitch());
                loc.add(20, 40, 0);
                Bukkit.broadcastMessage(ChatColor.GREEN + "Island: " + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ());
                if (ChunkGenerator.checkArea(loc, 35, false, false)) {
                    if (loc.getBlock().getBiome().name().contains("DESERT")) {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/is_desert"), true, loc);
                    } else if (loc.getBlock().getBiome().name().contains("SNOWY")) {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/is_snowly"), true, loc);
                    } else if (loc.getBlock().getBiome().name().contains("BADLANDS")) {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/is_mesa"), true, loc);
                    } else {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/is_normal"), true, loc);
                    }

                    if (location.getBlock().getBiome().name().contains("DESERT")) {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/base_desert"), true, new Location(location.getWorld(), location.getBlockX(), min + 3, location.getZ(), location.getYaw(), location.getPitch()));
                    } else if (location.getBlock().getBiome().name().contains("SNOWY")) {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/base_snowly"), true, new Location(location.getWorld(), location.getBlockX(), min + 3, location.getZ(), location.getYaw(), location.getPitch()));
                    } else if (location.getBlock().getBiome().name().contains("BADLANDS")) {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/base_mesa"), true, new Location(location.getWorld(), location.getBlockX(), min + 3, location.getZ(), location.getYaw(), location.getPitch()));
                    } else {
                        WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/base_normal"), true, new Location(location.getWorld(), location.getBlockX(), min + 3, location.getZ(), location.getYaw(), location.getPitch()));
                    }
                }
            } catch (WorldEditException | IOException e) {
                e.printStackTrace();
            }
        }, new Random().nextInt(200));
    }
}
