package com.mcm.plotgen.cache;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import java.util.ArrayList;
import java.util.HashMap;

public class EntitysCache {

    public static ArrayList<String> uuids;

    private String uuid;
    private Entity entity;
    private LivingEntity livingEntity;
    private Location location;
    private static HashMap<String, EntitysCache> cache = new HashMap<>();

    public EntitysCache(String uuid, Entity entity, LivingEntity livingEntity, Location location) {
        this.uuid = uuid;
        this.entity = entity;
        this.livingEntity = livingEntity;
        this.location = location;
    }

    public EntitysCache insert() {
        this.cache.put(this.uuid, this);
        if (uuids == null) uuids = new ArrayList<>();
        uuids.add(this.uuid);
        return this;
    }

    public static EntitysCache get(String uuid) {
        return cache.get(uuid);
    }

    public static void delete(String uuid) {
        cache.remove(uuid);
        uuids.remove(uuid);
    }

    public Entity getEntity() {
        return this.entity;
    }

    public Entity getLivEntity() {
        return this.livingEntity;
    }

    public Location getLocation() {
        return this.location;
    }
}
