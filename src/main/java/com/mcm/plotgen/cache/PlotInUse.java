package com.mcm.plotgen.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class PlotInUse {

    public static ArrayList<String> locals;

    private String local;
    private String uuid;
    private ArrayList<String> friends;
    private int plotSize;
    private static HashMap<String, PlotInUse> cache = new HashMap<>();

    public PlotInUse(String local, String uuid, int plotSize) {
        this.local = local;
        this.uuid = uuid;
        this.friends = new ArrayList<>();
        this.plotSize = plotSize;

        if (locals == null) locals = new ArrayList<>();
        locals.add(local);
    }

    public PlotInUse insert() {
        this.cache.put(this.local, this);
        return this;
    }

    public static PlotInUse get(String local) {
        return cache.get(local);
    }

    public void delete() {
        locals.remove(this.local);
        cache.remove(this.local);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getPlotSize() {
        return plotSize;
    }

    public void setPlotSize(int plotSize) {
        this.plotSize = plotSize;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void addFriend(String friend) {
        this.friends.add(friend);
    }
}
