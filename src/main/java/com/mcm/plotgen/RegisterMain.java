package com.mcm.plotgen;

import com.mcm.plotgen.listeners.PlayerEntryTowerArea;
import com.mcm.plotgen.utils.LoadAllPlots;
import org.bukkit.Bukkit;

public class RegisterMain {

    public static void register() {
        Bukkit.getWorld("world").getPopulators().add(new ChunkGenerator());
        LoadAllPlots.load();
        TaskAutoSave.run();

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                PlayerEntryTowerArea.spawnParticles();
            }
        }, 10L);
    }
}
