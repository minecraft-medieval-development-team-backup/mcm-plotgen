package com.mcm.plotgen;

import com.mcm.plotgen.commands.CommandPlotNext;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static String serverName;

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() {
        return Main.instance;
    }

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        RegisterMain.register();
        RegisterListeners.register();
        Main.instance.getCommand("plotnext").setExecutor(new CommandPlotNext());
        Main.instance.getCommand("testando").setExecutor(new CommandPlotNext());
        /*
        onPluginMessageSend();

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (String location : PlotsDb.getLocations(serverName)) {
                    String[] split = location.split(":");
                    PlotsLocation.get().addLocations(new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3])));
                }
            }
        }, 5L);
         */
    }

    @Override
    public void onDisable() {
        //-
    }
}

