package com.mcm.plotgen;

import com.mcm.plotgen.inventoryListeners.InventorySyncPlot;
import com.mcm.plotgen.listeners.PlayerEntryTowerArea;
import com.mcm.plotgen.listeners.PlayerInteractSign;
import com.mcm.plotgen.listeners.PlayerQuitEvents;
import com.mcm.plotgen.listeners.PlotEvents;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerEntryTowerArea(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlotEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractSign(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventorySyncPlot(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitEvents(), Main.plugin);
    }
}
