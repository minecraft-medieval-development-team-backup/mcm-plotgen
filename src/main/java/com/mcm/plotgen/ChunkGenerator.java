package com.mcm.plotgen;

import com.mcm.plotgen.cache.PlotsLocation;
import org.bukkit.*;
import org.bukkit.generator.BlockPopulator;

import java.util.Arrays;
import java.util.Random;

public class ChunkGenerator extends BlockPopulator {

    @Override
    public void populate(World world, Random random, Chunk chunk) {
        int x = chunk.getX() * 16;
        int z = chunk.getZ() * 16;
        int cx = x + random.nextInt(10);
        int cz = z + random.nextInt(10);

        int baseSize = 3;

        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            String xS = String.valueOf(x);
            String zS = String.valueOf(z);
            if (xS.length() > 3 && zS.length() > 3 && Integer.parseInt(xS.substring(xS.length() - 3)) <= 300 && Integer.parseInt(xS.substring(xS.length() - 3)) >= 100 && Integer.parseInt(zS.substring(zS.length() - 3)) <= 300 && Integer.parseInt(zS.substring(zS.length() - 3)) >= 100) {
                Location basePos = new Location(world, cx, world.getHighestBlockYAt(cx, cz), cz);
                basePos.add(-1, 0, -1);

                if (!hasPlacedNext(basePos) && !checkArea(basePos, 20, false, true)) {
                    int highest = basePos.getBlockY();
                    Location location = basePos.clone().add(-1, 0, -1);
                    location.setY(highest);

                    if (checkArea(location, 20, true, false)) return;

                    PlotsLocation.get(String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":" + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3)).place(location);
                }
            }
        }, new Random().nextInt(200));
    }

    public static boolean checkArea(Location location, int sizeCheck, boolean checkMountain, boolean checkUnwanted) {
        Location posStart = location.clone().add((int) -(sizeCheck / 2), (int) -(sizeCheck / 2), (int) -(sizeCheck / 2));

        if (checkUnwanted) {
            int yCheck = 3;
            boolean hasUnwanted = false;
            for (int y = 0; y <= yCheck; y++) {
                for (int x = 0; x <= sizeCheck; x++) {
                    for (int z = 0; z <= sizeCheck; z++) {
                        Location loc = new Location(location.getWorld(), posStart.getBlockX() + x, posStart.getBlockY(), posStart.getBlockZ() + z);
                        Location locTest = new Location(loc.getWorld(), loc.getBlockX(), loc.getWorld().getHighestBlockYAt(loc) - y, loc.getBlockZ());
                        if (!locTest.getBlock().isLiquid() && !locTest.getBlock().getType().name().contains("LOG") && !locTest.getBlock().getType().name().contains("LEAVE") && !locTest.getBlock().getType().name().contains("LILY") && !locTest.getBlock().getType().name().contains("ICE"))
                            ;
                        else hasUnwanted = true;
                        break;
                    }
                }
            }
            return hasUnwanted;
        } else if (checkMountain) {
            int maxDifference = 2;
            int maxY = location.getBlockY();
            int minY = location.getBlockY();
            for (int x = 0; x <= sizeCheck; x++) {
                for (int z = 0; z <= sizeCheck; z++) {
                    Location loc = new Location(location.getWorld(), posStart.getBlockX() + x, posStart.getBlockY(), posStart.getBlockZ() + z);
                    if (loc.getWorld().getHighestBlockYAt(loc) > maxY) maxY = loc.getWorld().getHighestBlockYAt(loc);
                    if (loc.getWorld().getHighestBlockYAt(loc) < minY) minY = loc.getWorld().getHighestBlockYAt(loc);
                }
            }
            if ((maxY - minY) <= maxDifference) return false;
            else return true;
        } else {
            boolean isClear = true;
            for (int y = 0; y <= sizeCheck; y++) {
                for (int x = 0; x <= sizeCheck; x++) {
                    for (int z = 0; z <= sizeCheck; z++) {
                        Location loc = new Location(posStart.getWorld(), posStart.getBlockX() + x, posStart.getBlockY() + y, posStart.getBlockZ() + z);
                        if (loc.getBlock() != null && !loc.getBlock().getType().equals(Material.AIR)) isClear = false;
                        break;
                    }
                }
            }
            return isClear;
        }
    }

    public static int getMin(Location location, int sizeCheck) {
        Location posStart = location.clone().add((int) -(sizeCheck / 2), (int) -(sizeCheck / 2), (int) -(sizeCheck / 2));

        int minY = location.getBlockY();
        for (int x = 0; x <= sizeCheck; x++) {
            for (int z = 0; z <= sizeCheck; z++) {
                Location loc = new Location(location.getWorld(), posStart.getBlockX() + x, posStart.getBlockY(), posStart.getBlockZ() + z);
                Material[] ignore = {Material.GRASS};
                if (loc.getWorld().getHighestBlockYAt(loc) < minY && !Arrays.asList(ignore).contains(new Location(loc.getWorld(), loc.getBlockX(), loc.getWorld().getHighestBlockYAt(loc), loc.getBlockZ()).getBlock().getType()))
                    minY = loc.getWorld().getHighestBlockYAt(loc);
            }
        }
        return minY;
    }

    private static boolean hasPlacedNext(Location location) {
        boolean has = false;
        String pos = String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":" + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3);
        if (PlotsLocation.get(pos) != null && PlotsLocation.get(pos).getLocation() != null) has = true;
        return has;
    }
}
