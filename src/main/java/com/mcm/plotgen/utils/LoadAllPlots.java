package com.mcm.plotgen.utils;

import com.mcm.core.Main;
import com.mcm.core.database.PlotsDb;
import com.mcm.plotgen.cache.PlotsLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import java.util.ArrayList;

public class LoadAllPlots {

    public static void load() {
        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
            ArrayList<String> plots = PlotsDb.getLocations(Main.server_name);

            if (!plots.isEmpty()) {
                for (String plot : plots) {
                    String[] split = plot.split(":");
                    Location location = new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));
                    String local = String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":"
                            + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3);
                    new PlotsLocation(local, location).insert();
                }
            }
        }, 10L);
    }
}
