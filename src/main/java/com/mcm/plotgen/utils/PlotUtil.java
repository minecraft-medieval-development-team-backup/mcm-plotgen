package com.mcm.plotgen.utils;

import com.mcm.plotgen.cache.PlotsLocation;
import org.bukkit.Location;

public class PlotUtil {

    public static boolean onPlot150(Location location) {
        PlotsLocation plot = PlotsLocation.get(String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":"
                + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3));
        if (plot != null) {
            final String[] pos1 = (plot.getLocation().getWorld().getName() + ":" + (plot.getLocation().getBlockX() + 41) + ":0:" + (plot.getLocation().getBlockZ() - 75)).split(":");
            final String[] pos2 = (plot.getLocation().getWorld().getName() + ":" + (plot.getLocation().getBlockX() + 191) + ":256:" + (plot.getLocation().getBlockZ() + 75)).split(":");

            if (pos1[0].equalsIgnoreCase(location.getWorld().getName())) {
                final int p1x = Integer.valueOf(pos1[1]);
                final int p1y = Integer.valueOf(pos1[2]);
                final int p1z = Integer.valueOf(pos1[3]);
                final int p2x = Integer.valueOf(pos2[1]);
                final int p2y = Integer.valueOf(pos2[2]);
                final int p2z = Integer.valueOf(pos2[3]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((location.getBlockX() >= minX) && (location.getBlockX() <= maxX) && (location.getBlockY() >= minY)
                        && (location.getBlockY() <= maxY) && (location.getBlockZ() >= minZ) && (location.getBlockZ() <= maxZ)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean onPlot100(Location location) {
        PlotsLocation plot = PlotsLocation.get(String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":"
                + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3));
        if (plot != null) {
            final String[] pos1 = (plot.getLocation().getWorld().getName() + ":" + (plot.getLocation().getBlockX() + 41) + ":0:" + (plot.getLocation().getBlockZ() - 75)).split(":");
            final String[] pos2 = (plot.getLocation().getWorld().getName() + ":" + (plot.getLocation().getBlockX() + 141) + ":256:" + (plot.getLocation().getBlockZ() + 25)).split(":");

            if (pos1[0].equalsIgnoreCase(location.getWorld().getName())) {
                final int p1x = Integer.valueOf(pos1[1]);
                final int p1y = Integer.valueOf(pos1[2]);
                final int p1z = Integer.valueOf(pos1[3]);
                final int p2x = Integer.valueOf(pos2[1]);
                final int p2y = Integer.valueOf(pos2[2]);
                final int p2z = Integer.valueOf(pos2[3]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((location.getBlockX() >= minX) && (location.getBlockX() <= maxX) && (location.getBlockY() >= minY)
                        && (location.getBlockY() <= maxY) && (location.getBlockZ() >= minZ) && (location.getBlockZ() <= maxZ)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean onIsland(Location location) {
        PlotsLocation plot = PlotsLocation.get(String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":"
                + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3));
        if (plot != null) {
            final String[] pos1 = (plot.getLocation().getWorld().getName() + ":" + (plot.getLocation().getBlockX() - 40) + ":0:" + (plot.getLocation().getBlockZ() - 40)).split(":");
            final String[] pos2 = (plot.getLocation().getWorld().getName() + ":" + (plot.getLocation().getBlockX() + 40) + ":256:" + (plot.getLocation().getBlockZ() + 40)).split(":");

            if (pos1[0].equalsIgnoreCase(location.getWorld().getName())) {
                final int p1x = Integer.valueOf(pos1[1]);
                final int p1y = Integer.valueOf(pos1[2]);
                final int p1z = Integer.valueOf(pos1[3]);
                final int p2x = Integer.valueOf(pos2[1]);
                final int p2y = Integer.valueOf(pos2[2]);
                final int p2z = Integer.valueOf(pos2[3]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((location.getBlockX() >= minX) && (location.getBlockX() <= maxX) && (location.getBlockY() >= minY)
                        && (location.getBlockY() <= maxY) && (location.getBlockZ() >= minZ) && (location.getBlockZ() <= maxZ)) {
                    return true;
                }
            }
        }
        return false;
    }
}
