package com.mcm.plotgen.commands;

import com.mcm.plotgen.Main;
import com.mcm.plotgen.cache.EntitysCache;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;

public class CommandPlotNext implements CommandExecutor {

    public static boolean checkIfAreaIsClear(Location location) {
        Location posStart = location.clone().add(-20, -20, -20);

        int sizeCheck = 30;
        boolean isClear = true;
        for (int y = 0; y <= sizeCheck; y++) {
            for (int x = 0; x <= sizeCheck; x++) {
                for (int z = 0; z <= sizeCheck; z++) {
                    Location loc = new Location(posStart.getWorld(), posStart.getBlockX() + x, posStart.getBlockY() + y, posStart.getBlockZ() + z);
                    if (y == 0 && x == 0 && z == 0) Bukkit.broadcastMessage(ChatColor.RED + "" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ());
                    if (y == sizeCheck && x == sizeCheck && z == sizeCheck) Bukkit.broadcastMessage(ChatColor.YELLOW + "" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ());
                    loc.getBlock().setType(Material.STONE);
                }
            }
        }
        return isClear;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (command.getName().equalsIgnoreCase("testando")) {
            checkIfAreaIsClear(player.getLocation().add(0, 40, 0));
        }

        if (command.getName().equalsIgnoreCase("plotnext")) {
            //ArmorStand armorStand = (ArmorStand) player.getWorld().spawnEntity(new Location(player.getWorld(), 10, player.getWorld().getHighestBlockYAt(player.getLocation().clone().getBlockX(), player.getLocation().clone().getBlockZ()), 0), EntityType.ARMOR_STAND);
            Entity entity = player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE);
            Zombie zombie = (Zombie) entity;
            zombie.setBaby(true);
            new EntitysCache(uuid, entity, null, null).insert();

            int task = Bukkit.getScheduler().scheduleAsyncRepeatingTask(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    //O que vai repetir aqui.
                }
            }, 20L, 20L);
            Bukkit.getScheduler().cancelTask(task);
            /*
            Location location = getPlotNext(player.getLocation(), uuid);

            if (location != null) {
               //
                player.sendMessage(ChatColor.YELLOW + " * Siga a entidade até o plot!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, não existe nenhum plot próximo.");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
             */
        }
        return false;
    }

    /*
    private static Location getPlotNext(Location location, String uuid) {
        if (PlotsLocation.get(String.valueOf(location.getBlockX()).substring(0, String.valueOf(location.getBlockX()).length() - 3) + ":" + String.valueOf(location.getBlockZ()).substring(0, String.valueOf(location.getBlockZ()).length() - 3)).getLocations() != null) {
            ArrayList<Double> distances = new ArrayList<>();
            double distance = 999999999;
            for (Location loc : PlotsLocation.get(String.valueOf(location.getBlockX()).substring(0, 1) + ":" + String.valueOf(location.getBlockZ()).substring(0, 1)).getLocations()) {
                if (location.distance(loc) < distance) {
                    distance = location.distance(loc);
                    distances.add(location.distance(loc));
                    new PlotsNext(uuid + distance, loc).insert();
                }
            }

            Location loc = PlotsNext.get(uuid + distance).getLocation().clone();
            for (double d : distances) {
                PlotsNext.delete(uuid + d);
            }
            return loc;
        }
        return null;
    }

     */
}
