package com.mcm.plotgen.managers;

import com.mcm.core.Main;
import com.mcm.core.database.HomeDb;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventorySyncPlot {

    public static int plotPrice = 30000;
    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory get(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("vG!H#f6$&kHb3?U", uuid));

        if (HomeDb.haveHome(uuid)) {
            ItemStack sync = new ItemStack(Material.GRASS_BLOCK);
            ItemMeta metaSync = sync.getItemMeta();
            metaSync.setDisplayName(Main.getTradution("Pg9bjep&f3tX7N!", uuid));
            ArrayList<String> loreSync = new ArrayList<>();
            loreSync.add(Main.getTradution("6$E$SsYT$nf58fy", uuid));
            loreSync.add(Main.getTradution("6wRNHk33@bSEQ33", uuid));
            loreSync.add(Main.getTradution("S3T2pp%jRvAXU8&", uuid));
            loreSync.add(Main.getTradution("FYwX##2vNjgzzZr", uuid));
            loreSync.add(" ");
            loreSync.add(Main.getTradution("YAk5ErYK$?%Xey#", uuid));
            loreSync.add(Main.getTradution("H&Q8dmj@X?qDR@4", uuid));
            loreSync.add(" ");
            loreSync.add(Main.getTradution("8NtnfFJb@kdv@aY", uuid));
            metaSync.setLore(loreSync);
            sync.setItemMeta(metaSync);

            ItemStack config = new ItemStack(Material.COMPARATOR);
            ItemMeta metaConfig = config.getItemMeta();
            metaConfig.setDisplayName(ChatColor.GREEN + "Configurar");
            ArrayList<String> loreConfig = new ArrayList<>();
            loreConfig.add(" ");
            loreConfig.add(Main.getTradution("S9xfBbs$T7G%fKq", uuid));
            metaConfig.setLore(loreConfig);
            config.setItemMeta(metaConfig);

            inventory.setItem(12, sync);
            inventory.setItem(14, EnchantmentGlow.addGlow(RemoveAllFlags.remove(config)));
            return inventory;
        } else {
            ItemStack buy = new ItemStack(Material.GRASS_BLOCK);
            ItemMeta metaBuy = buy.getItemMeta();
            metaBuy.setDisplayName(Main.getTradution("E2Sjz6xmHazYPb!", uuid));
            ArrayList<String> loreBuy = new ArrayList<>();
            loreBuy.add(Main.getTradution("!NtqAQZ5HsTcwzS", uuid));
            loreBuy.add(Main.getTradution("@wRsXPxM7FGAV3W", uuid));
            loreBuy.add(Main.getTradution("Q*5EhR5fKA@UTPy", uuid));
            loreBuy.add(Main.getTradution("6u?byCJmQ5X?HhV", uuid));
            loreBuy.add(Main.getTradution("v82v@THXFaDY?t#", uuid));
            loreBuy.add(Main.getTradution("Y4CkC4Qn*xH#phP", uuid));
            loreBuy.add(Main.getTradution("f@xRQGf2V4Sm9n@", uuid));
            loreBuy.add(Main.getTradution("aAn9BmH*uuN$U7w", uuid));
            loreBuy.add(" ");
            loreBuy.add(Main.getTradution("gR*#hft75YqvmGZ", uuid) + ChatColor.GOLD + "$" + formatter.format(plotPrice));
            loreBuy.add(" ");
            loreBuy.add(Main.getTradution("Ng?uV!d7hJynxSq", uuid));
            metaBuy.setLore(loreBuy);
            buy.setItemMeta(metaBuy);

            inventory.setItem(13, buy);
            return inventory;
        }
    }
}
