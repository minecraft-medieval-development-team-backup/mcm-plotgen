package com.mcm.plotgen.listeners;

import com.mcm.plotgen.cache.EntitysCache;
import com.mcm.plotgen.cache.PlotsLocation;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.concurrent.ThreadLocalRandom;

public class PlayerEntryTowerArea implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (inArea(player.getLocation()) != null && player.getLocation().clone().add(0, -1, 0).getBlock().getType().equals(Material.SLIME_BLOCK)) {
            Vector vector = player.getLocation().getDirection();
            vector.setY(3);
            player.setVelocity(vector);
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SHOOT, 1.0f, 1.0f);
        }
    }

    public static void spawnParticles() {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(750);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (EntitysCache.uuids != null && EntitysCache.uuids.contains(player.getUniqueId().toString())) {
                        for (Entity entity : player.getNearbyEntities(15, 15, 15)) {
                            if (entity instanceof ArmorStand) {
                                Zombie zombie = (Zombie) EntitysCache.get(player.getUniqueId().toString()).getEntity();
                                zombie.setTarget((LivingEntity) entity);
                            }
                        }
                    }

                    Location plot = inArea(player.getLocation());

                    if (plot != null) {
                        Location corner = plot.clone().add(1, 0, 1);
                        Location particleAngry = plot.clone();
                        particleAngry.add(0, 3, 0);

                        int baseSize = 3;
                        for (int x = 0; x < baseSize; x++) {
                            for (int z = 0; z < baseSize; z++) {
                                double angryX = ThreadLocalRandom.current().nextDouble(plot.getBlockX() - 0.800, plot.getBlockX() + 0.800);
                                double angryZ = ThreadLocalRandom.current().nextDouble(plot.getBlockZ() - 0.800, plot.getBlockZ() + 0.800);
                                player.spawnParticle(Particle.VILLAGER_ANGRY, new Location(player.getWorld(), angryX, particleAngry.getY(), angryZ), 1);
                                particleAngry.add(0, 1, 0);

                                for (int a = 0; a <= 5; a++) {
                                    double randomX = ThreadLocalRandom.current().nextDouble(((corner.getBlockX() + 0.5) - x) - 0.800, ((corner.getBlockX() + 0.5) - x) + 0.800);
                                    double randomZ = ThreadLocalRandom.current().nextDouble(((corner.getBlockZ() + 0.5) - z) - 0.800, ((corner.getBlockZ() + 0.5) - z) + 0.800);
                                    Location location = new Location(player.getWorld(), randomX, plot.getY(), randomZ);
                                    player.spawnParticle(Particle.VILLAGER_HAPPY, location, 1);
                                }
                            }
                        }
                    }
                }
            }
        });
        thread.start();
        thread.setName("thread_plot_particles");
    }

    public static Location inArea(final Location loc) {
        String pos = String.valueOf(loc.getBlockX()).substring(0, String.valueOf(loc.getBlockX()).length() - 3) + ":" + String.valueOf(loc.getBlockZ()).substring(0, String.valueOf(loc.getBlockZ()).length() - 3);
        Location location = PlotsLocation.get(pos).getLocation();
        if (location != null) {
            final String[] pos1 = (location.getWorld().getName() + ":" + (location.getBlockX() - 50) + ":0:" + (location.getBlockZ() - 50)).split(":");
            final String[] pos2 = (location.getWorld().getName() + ":" + (location.getBlockX() + 50) + ":256:" + (location.getBlockZ() + 50)).split(":");

            if (pos1[0].equalsIgnoreCase(loc.getWorld().getName())) {
                final int p1x = Integer.valueOf(pos1[1]);
                final int p1y = Integer.valueOf(pos1[2]);
                final int p1z = Integer.valueOf(pos1[3]);
                final int p2x = Integer.valueOf(pos2[1]);
                final int p2y = Integer.valueOf(pos2[2]);
                final int p2z = Integer.valueOf(pos2[3]);

                final int minX = p1x < p2x ? p1x : p2x;
                final int minY = p1y < p2y ? p1y : p2y;
                final int minZ = p1z < p2z ? p1z : p2z;

                final int maxX = p1x > p2x ? p1x : p2x;
                final int maxY = p1y > p2y ? p1y : p2y;
                final int maxZ = p1z > p2z ? p1z : p2z;

                if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                        && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {
                    return location;
                }
            }
        }
        return null;
    }
}
