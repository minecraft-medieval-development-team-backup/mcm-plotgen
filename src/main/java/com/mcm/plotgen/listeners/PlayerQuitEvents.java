package com.mcm.plotgen.listeners;

import com.mcm.core.database.HomeDb;
import com.mcm.plotgen.Main;
import com.mcm.plotgen.cache.PlotInUse;
import com.mcm.plotgen.cache.PlotsLocation;
import com.mcm.plotgen.utils.WorldEditUtil;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.IOException;

public class PlayerQuitEvents implements Listener {

    /**
     * TODO * Check the probability of the suffocation of the player!
     *
     * @param event
     */
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if (PlotInUse.locals != null) {
            for (String local : PlotInUse.locals) {
                if (PlotInUse.get(local).getUuid().equals(event.getPlayer().getUniqueId().toString())) {
                        try {
                            Location pos1 = new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 41, 1, PlotsLocation.get(local).getLocation().clone().getBlockZ() - 75);
                            Location pos2 = new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 191, 256, PlotsLocation.get(local).getLocation().clone().getBlockZ() + 75);
                            WorldEditUtil.save("plugins/mcm-plotsgen/plots/" + PlotInUse.get(local).getUuid(), WorldEditUtil.copy(WorldEditUtil.getRegion(pos1, pos2)));

                            //ADD HERE RABBITMQ/DB SAVE

                            WorldEditUtil.paste(WorldEditUtil.load("plugins/mcm-plotsgen/old-locals/" + local), true, new Location(PlotsLocation.get(local).getLocation().clone().getWorld(), PlotsLocation.get(local).getLocation().clone().getBlockX() + 41, 1, PlotsLocation.get(local).getLocation().clone().getBlockZ() - 75));
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (WorldEditException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    }
}
