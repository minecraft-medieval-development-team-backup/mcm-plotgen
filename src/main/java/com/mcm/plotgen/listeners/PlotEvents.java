package com.mcm.plotgen.listeners;

import com.mcm.core.Main;
import com.mcm.plotgen.cache.PlotInUse;
import com.mcm.plotgen.utils.PlotUtil;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlotEvents implements Listener {

    /**
     * Removing explosion grif
     * @param event
     */
    @EventHandler
    public void onExplode(BlockExplodeEvent event) {
        for (org.bukkit.block.Block block : event.blockList()) {
            if (PlotUtil.onPlot100(block.getLocation()) || PlotUtil.onPlot150(block.getLocation())) {
                event.blockList().remove(block);
            }
        }
    }

    /**
     * Removing interaction of the other players with armorstand
     * @param event
     */
    @EventHandler
    public void onClickArmorStand(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getRightClicked() instanceof ArmorStand) {
            if (PlotUtil.onPlot150(event.getRightClicked().getLocation()) || PlotUtil.onPlot100(event.getRightClicked().getLocation())) {
                String local = String.valueOf(event.getRightClicked().getLocation().getBlockX()).substring(0, String.valueOf(event.getRightClicked().getLocation().getBlockX()).length() - 3) + ":"
                        + String.valueOf(event.getRightClicked().getLocation().getBlockZ()).substring(0, String.valueOf(event.getRightClicked().getLocation().getBlockZ()).length() - 3);

                if (PlotInUse.get(local) != null) {
                    if (PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid) || PlotInUse.get(local).getFriends().contains(uuid)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        sendMsgCanceled(player, "CANCELAR");
                    }
                }
            }
        }
    }

    /**
     * Removing the probability of the other players break "item_frame and armor_stand" on plot of the other owner
     * @param event
     */
    @EventHandler
    public void onInteractEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            String uuid = player.getUniqueId().toString();

            if (event.getEntity().getType().equals(EntityType.ITEM_FRAME) || event.getEntity().getType().equals(EntityType.ARMOR_STAND)) {
                if (PlotUtil.onPlot150(event.getEntity().getLocation()) || PlotUtil.onPlot100(event.getEntity().getLocation())) {
                    String local = String.valueOf(event.getEntity().getLocation().getBlockX()).substring(0, String.valueOf(event.getEntity().getLocation().getBlockX()).length() - 3) + ":"
                            + String.valueOf(event.getEntity().getLocation().getBlockZ()).substring(0, String.valueOf(event.getEntity().getLocation().getBlockZ()).length() - 3);

                    if (PlotInUse.get(local) != null) {
                        if (PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid) || PlotInUse.get(local).getFriends().contains(uuid)) {
                            return;
                        } else {
                            event.setCancelled(true);
                            sendMsgCanceled(player, "CANCELAR");
                        }
                    }
                }
            }
        }
    }

    /**
     * Removing when projectile hits a entity on plot, if the shooter don't has the permission
     * @param event
     */
    @EventHandler
    public void onProjectile(ProjectileHitEvent event) {
        if (event.getHitEntity() != null && event.getEntity().getShooter() instanceof Player) {
            if (PlotUtil.onPlot150(event.getHitEntity().getLocation()) || PlotUtil.onPlot100(event.getHitEntity().getLocation())) {
                String local = String.valueOf(event.getHitEntity().getLocation().getBlockX()).substring(0, String.valueOf(event.getHitEntity().getLocation().getBlockX()).length() - 3) + ":"
                        + String.valueOf(event.getHitEntity().getLocation().getBlockZ()).substring(0, String.valueOf(event.getHitEntity().getLocation().getBlockZ()).length() - 3);

                if (PlotInUse.get(local) != null) {
                    Player player = (Player) event.getEntity().getShooter();
                    String uuid = player.getUniqueId().toString();

                    if (PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid) || PlotInUse.get(local).getFriends().contains(uuid)) {
                        return;
                    } else {
                        event.getEntity().remove();
                    }
                }
            }
        }
    }

    /**
     * Checking if the player that is trying entry on plot has permission for this
     * @param event
     */
    @EventHandler
    public void onEntry(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
    }

    /**
     * Checking if the player that break the block has the permission for this
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        String local = String.valueOf(player.getLocation().getBlockX()).substring(0, String.valueOf(player.getLocation().getBlockX()).length() - 3) + ":"
                + String.valueOf(player.getLocation().getBlockZ()).substring(0, String.valueOf(player.getLocation().getBlockZ()).length() - 3);

        /**
         * Checking the event occurred on plot size 100
         */
        if (PlotUtil.onPlot100(event.getBlock().getLocation())) {
            if (PlotInUse.get(local) != null && PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid)) {
                return;
            } else {
                if (PlotInUse.get(local) != null && PlotInUse.get(local).getFriends().contains(uuid)) return;
                event.setCancelled(true);
                sendMsgCanceled(player, "CANCELAR");
            }
        }

        /**
         * Checking the event occurred on plot size 150
         */
        if (PlotUtil.onPlot150(event.getBlock().getLocation())) {
            if (PlotInUse.get(local) != null && PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid)) {
                if (PlotInUse.get(local).getPlotSize() == 100 && !PlotUtil.onPlot100(event.getBlock().getLocation())) {
                    event.setCancelled(true);
                    sendMsgCanceled(player, "CANCELAR E NOTIFICAR VANTAGEM VIP");
                }
            } else {
                if (PlotInUse.get(local) != null && PlotInUse.get(local).getFriends().contains(uuid)) {
                    if (PlotInUse.get(local).getPlotSize() == 100 && !PlotUtil.onPlot100(event.getBlock().getLocation())) {
                        event.setCancelled(true);
                        sendMsgCanceled(player, "CANCELAR E NOTIFICAR VANTAGEM VIP");
                    }
                    return;
                }
                event.setCancelled(true);
                sendMsgCanceled(player, "CANCELAR");
            }
        }

        /**
         * Cancelling any interactions with island area.
         */
        if (PlotUtil.onIsland(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    /**
     * Checking if the player that place the block has the permission for this
     * @param event
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        String local = String.valueOf(player.getLocation().getBlockX()).substring(0, String.valueOf(player.getLocation().getBlockX()).length() - 3) + ":"
                + String.valueOf(player.getLocation().getBlockZ()).substring(0, String.valueOf(player.getLocation().getBlockZ()).length() - 3);

        /**
         * Checking the event occurred on plot size 100
         */
        if (PlotUtil.onPlot100(event.getBlock().getLocation())) {
            if (PlotInUse.get(local) != null && PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid)) {
                return;
            } else {
                if (PlotInUse.get(local) != null && PlotInUse.get(local).getFriends().contains(uuid)) return;
                event.setCancelled(true);
                sendMsgCanceled(player, "CANCELAR");
            }
        }

        /**
         * Checking the event occurred on plot size 150
         */
        if (PlotUtil.onPlot150(event.getBlock().getLocation())) {
            if (PlotInUse.get(local) != null && PlotInUse.get(local).getUuid().equalsIgnoreCase(uuid)) {
                if (PlotInUse.get(local).getPlotSize() == 100 && !PlotUtil.onPlot100(event.getBlock().getLocation())) {
                    event.setCancelled(true);
                    sendMsgCanceled(player, "CANCELAR E NOTIFICAR VANTAGEM VIP");
                }
            } else {
                if (PlotInUse.get(local) != null && PlotInUse.get(local).getFriends().contains(uuid)) {
                    if (PlotInUse.get(local).getPlotSize() == 100 && !PlotUtil.onPlot100(event.getBlock().getLocation())) {
                        event.setCancelled(true);
                        sendMsgCanceled(player, "CANCELAR E NOTIFICAR VANTAGEM VIP");
                    }
                    return;
                }
                event.setCancelled(true);
                sendMsgCanceled(player, "CANCELAR");
            }
        }

        /**
         * Cancelling any interactions with island area.
         */
        if (PlotUtil.onIsland(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    private static void sendMsgCanceled(Player player, String message_id) {
        player.sendMessage(Main.getTradution(message_id, player.getUniqueId().toString()));
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
    }
}
