package com.mcm.plotgen.listeners;

import com.mcm.plotgen.cache.PlotsLocation;
import com.mcm.plotgen.managers.InventorySyncPlot;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractSign implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getAction().name().equalsIgnoreCase("RIGHT_CLICK_BLOCK") && event.getClickedBlock() != null && event.getClickedBlock().getType().name().contains("SIGN")) {
            String local = String.valueOf(player.getLocation().getBlockX()).substring(0, String.valueOf(player.getLocation().getBlockX()).length() - 3) + ":"
                    + String.valueOf(player.getLocation().getBlockZ()).substring(0, String.valueOf(player.getLocation().getBlockZ()).length() - 3);
            if (PlotsLocation.get(local) != null) {
                Location location = PlotsLocation.get(local).getLocation();
                Location loc = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), location.getYaw(), location.getPitch());
                loc.add(16, 37, 0);

                if ((event.getClickedBlock().getLocation().getWorld().getName() + ":" + event.getClickedBlock().getLocation().getBlockX() + ":" + event.getClickedBlock().getLocation().getBlockY() + ":" + event.getClickedBlock().getLocation().getBlockZ()).equals(loc.getWorld().getName() + ":" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ())) {
                    player.openInventory(InventorySyncPlot.get(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                }
            }
        }
    }
}
